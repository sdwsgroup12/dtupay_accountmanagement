package dtuPay.service.model;

import java.io.Serializable;
import java.util.Objects;

public class ValidatedTokenWithAccountId implements Serializable {
    private static final long serialVersionUID = 6181801171602486503L;
    private String customerAccountID;
    private ValidatedToken validatedToken;

    public String getCustomerAccountID() {
        return customerAccountID;
    }

    public void setCustomerAccountID(String customerAccountID) {
        this.customerAccountID = customerAccountID;
    }

    public ValidatedToken getValidatedToken() {
        return validatedToken;
    }

    public void setValidatedToken(ValidatedToken validatedToken) {
        this.validatedToken = validatedToken;
    }
}
