package dtuPay.service;

import dtuPay.service.model.*;
import messaging.Event;
import messaging.MessageQueue;

// @author Cesare s232435
public class AccountManagementService {

    private MessageQueue queue;
    private AccountRepository repository = AccountRepository.getInstance();
    public AccountManagementService(MessageQueue q) {
        queue = q;
        queue.addHandler("PaymentRequested", this::handlePaymentRequested);
        queue.addHandler("TokenValidated", this::handleTokenValidated);

        queue.addHandler("CustomerRegistrationRequested", this::handleCustomerRegistrationEvent);
        queue.addHandler("MerchantRegistrationRequested", this::handleMerchantRegistrationEvent);

        queue.addHandler("CustomerDeregistrationRequested", this::handleCustomerDeregistrationEvent);
        queue.addHandler("MerchantDeregistrationRequested", this::handleMerchantDeregistrationEvent);

        queue.addHandler("GetMerchantRequested", this::handleGetMerchantEvent);
        queue.addHandler("GetCustomerRequested", this::handleGetCustomerEvent);
    }

    public void handlePaymentRequested(Event e) {
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        PaymentRequest paymentRequest = e.getArgument(0,PaymentRequest.class);
        String merchantId = paymentRequest.getMerchantID();
        Client merchant;
        try {
            merchant = repository.getMerchant(merchantId);
        } catch (Exception exception) {
            queue.publish(new Event("MerchantBankAccountNotRetrieved",new Object[]{exception.getMessage(),correlationId}));
            return;
        }
        PaymentRequestWithAccount paymentRequestWithAccount = new PaymentRequestWithAccount();
        paymentRequestWithAccount.setPaymentRequest(paymentRequest);
        paymentRequestWithAccount.setMerchantAccountId(merchant.getAccountId());
        queue.publish(new Event("MerchantBankAccountRetrieved",new Object[]{paymentRequestWithAccount,correlationId}));
    }

    public void handleTokenValidated(Event e){
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        ValidatedToken validatedToken = e.getArgument(0,ValidatedToken.class);
        String customerId = validatedToken.getCustomerID();
        Client customer;
        try {
            customer = repository.getCustomer(customerId);
        } catch (Exception exception){
            //Cannot happen, as otherwise token would not be validated
            customer = new Client();
        }
        ValidatedTokenWithAccountId validatedTokenWithAccountId = new ValidatedTokenWithAccountId();
        validatedTokenWithAccountId.setCustomerAccountID(customer.getAccountId());
        validatedTokenWithAccountId.setValidatedToken(validatedToken);
        queue.publish(new Event("CustomerBankAccountRetrieved",new Object[]{validatedTokenWithAccountId,correlationId}));
    }

    public void handleCustomerRegistrationEvent(Event e) {
        Client requestedCustomer = e.getArgument(0, Client.class);
        repository.registerCustomer(requestedCustomer);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        Event event = new Event("CustomerRegistered", new Object[] { requestedCustomer, correlationId });
        queue.publish(event);
    }

    public void handleMerchantRegistrationEvent(Event e) {
        Client requestedMerchant = e.getArgument(0, Client.class);
        repository.registerMerchant(requestedMerchant);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        Event event = new Event("MerchantRegistered", new Object[] { requestedMerchant, correlationId });
        queue.publish(event);
    }

    public void handleCustomerDeregistrationEvent(Event e) {
        String requestedCustomerId = e.getArgument(0, String.class);
        repository.deregisterCustomer(requestedCustomerId);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        Event event = new Event("CustomerDeregistered", new Object[] { requestedCustomerId, correlationId });
        queue.publish(event);
    }

    public void handleMerchantDeregistrationEvent(Event e) {
        String requestedMerchantId = e.getArgument(0, String.class);
        repository.deregisterMerchant(requestedMerchantId);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        Event event = new Event("MerchantDeregistered", new Object[] { requestedMerchantId, correlationId });
        queue.publish(event);
    }

    public void handleGetMerchantEvent(Event e) {
        String clientId = e.getArgument(0, String.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);

        try {
            Client merchant = repository.getMerchant(clientId);
            Event event = new Event("MerchantRetrieved", new Object[] { merchant, correlationId });
            queue.publish(event);
        } catch (Exception exception) {
            Event event = new Event("MerchantNotRetrieved", new Object[] { exception.getMessage(), correlationId });
            queue.publish(event);
        }
    }

    public void handleGetCustomerEvent(Event e) {
        String clientId = e.getArgument(0, String.class);
        CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
        try {
            Client customer = repository.getCustomer(clientId);
            Event event = new Event("CustomerRetrieved", new Object[] { customer, correlationId });
            queue.publish(event);
        } catch (Exception exception) {
            Event event = new Event("CustomerNotRetrieved", new Object[] { exception.getMessage(), correlationId });
            queue.publish(event);
        }
    }
}
