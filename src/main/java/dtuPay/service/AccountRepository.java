package dtuPay.service;

import dtuPay.service.model.Client;

import java.util.ArrayList;
import java.util.List;


// @author Abi s205720
public class AccountRepository {

    private static AccountRepository repository = null;

    private final List<Client> customers = new ArrayList<>();
    private final List<Client> merchants = new ArrayList<>();

    public AccountRepository() {
    }

    public static AccountRepository getInstance() {
        if (repository == null) {
            repository = new AccountRepository();
        }
        return repository;
    }

    public void registerCustomer(Client customer) {
        customer.generateId();
        customers.add(customer);
    }

    public void registerMerchant(Client merchant) {
        merchant.generateId();
        merchants.add(merchant);
    }

    public void deregisterCustomer(String customerId) {
        customers.removeIf(customer -> customer.getId().equals(customerId));
    }

    public void deregisterMerchant(String merchantId) {
        merchants.removeIf(merchant -> merchant.getId().equals(merchantId));
    }

    public Client getCustomer(String id) throws Exception {
        for (Client customer : customers) {
            if (customer.getId().equals(id)) {
                return customer;
            }
        }
        throw new Exception("Customer not found");
    }

    public Client getCustomerByAccountId(String accountId) throws Exception {
        for (Client customer : customers) {
            if (customer.getAccountId().equals(accountId)) {
                return customer;
            }
        }
        throw new Exception("Customer not found");
    }

    public Client getMerchant(String id) throws Exception {
        for (Client merchant : merchants) {
            if (merchant.getId().equals(id)) {
                return merchant;
            }
        }
        throw new Exception("Merchant not found");
    }

    public Client getMerchantByAccountId(String accountId) throws Exception {
        for (Client merchant : merchants) {
            if (merchant.getAccountId().equals(accountId)) {
                return merchant;
            }
        }
        throw new Exception("Merchant not found");
    }

}
