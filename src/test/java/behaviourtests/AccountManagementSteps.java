package behaviourtests;

import dtuPay.service.AccountManagementService;
import dtuPay.service.AccountRepository;
import dtuPay.service.model.*;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;

// @author Can s232851
public class AccountManagementSteps {

    AccountRepository repo = AccountRepository.getInstance();
    Client merchant, customer, newMerchant, newCustomer;
    Event eventPaymentRequested, eventTokenValidated, eventTokenNotValidated, eventMerchantRegistrationRequested,
            eventCustomerRegistrationRequested, eventCustomerDeregistraionRequested,
            eventMerchantDeregistraionRequested;
    Exception exception;
    private CorrelationId paymentRequestedAndTokenValidated = CorrelationId.randomId();;
    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
    private final CompletableFuture<String> consumedEvent = new CompletableFuture<>();
    
    private MessageQueue q = new MessageQueue() {

        @Override
        public void publish(Event event) {
            publishedEvent.complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
            consumedEvent.complete(eventType);
        }
    };
    AccountManagementService service = new AccountManagementService(q);

    @Before
    public void setup() {
        merchant = new Client();
        merchant.setAccountId("111");
        merchant.setCpr("123");
        merchant.setName("joe merchant");
        repo.registerMerchant(merchant);

        customer = new Client();
        customer.setAccountId("222");
        customer.setCpr("456");
        customer.setName("joe customer");
        repo.registerCustomer(customer);
    }

    @Given("there is a {string} event received")
    public void there_is_a_event_received(String eventName) {

        switch (eventName) {
            case "PaymentRequested":
                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.setMerchantID(merchant.getId());
                paymentRequest.setAmount(100);
                eventPaymentRequested = new Event("PaymentRequested", new Object[] { paymentRequest, paymentRequestedAndTokenValidated });
                break;
            case "PaymentRequestFailed":
                PaymentRequest paymentRequestFailed = new PaymentRequest();
                paymentRequestFailed.setMerchantID(merchant.getId());
                paymentRequestFailed.setAmount(100);
                eventPaymentRequested = new Event("PaymentRequestFailed", new Object[] { paymentRequestFailed, paymentRequestedAndTokenValidated });
                break;
            case "TokenValidated":
                ValidatedToken tokenValidated = new ValidatedToken();
                tokenValidated.setCustomerID(customer.getId());

                eventTokenValidated = new Event("TokenValidated", new Object[] { tokenValidated, paymentRequestedAndTokenValidated });
                break;
            case "TokenNotValidated":
                ValidatedToken tokenNotValidated = new ValidatedToken();
                tokenNotValidated.setCustomerID(customer.getId());

                eventTokenNotValidated = new Event("TokenNotValidated", new Object[] { tokenNotValidated, paymentRequestedAndTokenValidated });
                break;

            case "GetCustomerRequested":
                publishedEvent = new CompletableFuture<>();
                Event eventGetCustomerRequested = new Event("GetCustomerRequested", new Object[] { newCustomer.getId(), CorrelationId.randomId() });
                service.handleGetCustomerEvent(eventGetCustomerRequested);
                break;

            case "GetMerchantRequested":
                publishedEvent = new CompletableFuture<>();
                Event eventGetMerchantRequested = new Event("GetMerchantRequested", new Object[] { newMerchant.getId(), CorrelationId.randomId() });
                service.handleGetMerchantEvent(eventGetMerchantRequested);
                break;
            default:
                // Handle unknown event name
                break;
        }
    }

    @When("the PaymentRequested event is handled")
    public void the_PaymentRequested_event_is_handled() {
        service.handlePaymentRequested(eventPaymentRequested);
    }

    @When("the TokenValidated event is handled")
    public void the_TokenValidated_event_is_handled() {
        service.handleTokenValidated(eventTokenValidated);
    }

    @Then("the merchant bank number and is retrieved")
    public void the_merchant_bank_number_and_is_retrieved() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then("the customer bank number is retrieved")
    public void the_customer_bank_number_is_retrieved() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then("the {string} event is published")
    public void the_event_is_published(String eventName) {
        Event event = publishedEvent.join();
        switch (eventName){
            case "CustomerRegistered":
                newCustomer = event.getArgument(0, Client.class);
                break;
            case "MerchantRegistered":
                newMerchant = event.getArgument(0, Client.class);
                break;
            default:
                break;
        }
        assertEquals(eventName, event.getType());
    }

    @Given("there is a customer not registered with CPR {string}, name {string}, account id {string}")
    public void there_is_a_customer_not_registered_with_cpr_name_account_id(String cpr, String name, String accountId) {
        newCustomer = new Client();
        newCustomer.setCpr(cpr);
        newCustomer.setName(name);
        newCustomer.setAccountId(accountId);
    }

    @When("the customer requests an account registration")
    public void the_customer_requests_an_account_registration() {
        eventCustomerRegistrationRequested = new Event("CustomerRegistrationRequested", new Object[] { newCustomer, CorrelationId.randomId() });
        publishedEvent = new CompletableFuture<>();
        service.handleCustomerRegistrationEvent(eventCustomerRegistrationRequested);
    }

    @Then("the customer account is successfully created")
    public void the_customer_account_is_successfully_created() {
        try {
            String accountId = newCustomer.getAccountId();
            Client createdCustomer = repo.getCustomerByAccountId(accountId);
            assertEquals(newCustomer.getCpr(), createdCustomer.getCpr());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    @Given("there is a merchant not registered with CPR {string}, name {string}, account id {string}")
    public void there_is_a_merchant_not_registered_with_cpr_name_account_id(String cpr, String name, String accountId) {
        newMerchant = new Client();
        newMerchant.setCpr(cpr);
        newMerchant.setName(name);
        newMerchant.setAccountId(accountId);
    }

    @When("the merchant requests an account registration")
    public void the_merchant_requests_an_account_registration() {
        eventMerchantRegistrationRequested = new Event("MerchantRegistrationRequested", new Object[] { newMerchant, CorrelationId.randomId() });
        service.handleMerchantRegistrationEvent(eventMerchantRegistrationRequested);
    }

    @Then("the merchant account is successfully created")
    public void the_merchant_account_is_successfully_created() {
        try {
            String accountId = newMerchant.getAccountId();
            Client createdMerchant = repo.getMerchantByAccountId(accountId);
            assertEquals(newMerchant.getCpr(), createdMerchant.getCpr());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("there is a customer registered with CPR {string}, name {string}, account id {string}")
    public void there_is_a_customer_registered_with_cpr_name_account_id(String cpr, String name, String accountId) {
        newCustomer = new Client();
        newCustomer.setCpr(cpr);
        newCustomer.setName(name);
        newCustomer.setAccountId(accountId);

        repo.registerCustomer(newCustomer);

        try {
            Client verifyCustomer = repo.getCustomerByAccountId(accountId);
            assertEquals(cpr, verifyCustomer.getCpr());
            assertEquals(name, verifyCustomer.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("the customer requests the account deregistration")
    public void the_customer_requests_the_account_deregistration() {
        eventCustomerDeregistraionRequested = new Event("CustomerDeregistrationRequested", new Object[] { newCustomer.getId(), CorrelationId.randomId() });
        service.handleCustomerDeregistrationEvent(eventCustomerDeregistraionRequested);
    }

    @Then("the customer account is successfully deleted")
    public void the_customer_account_is_successfully_deleted() {
        String accountId = newCustomer.getAccountId();
        try {
            repo.getCustomerByAccountId(accountId);
        } catch (Exception e) {
            assertEquals("Customer not found", e.getMessage());
        }
    }

    @Given("there is a merchant registered with CPR {string}, name {string}, account id {string}")
    public void there_is_a_merchant_registered_with_cpr_name_account_id(String cpr, String name, String accountId) {
        newMerchant = new Client();
        newMerchant.setCpr(cpr);
        newMerchant.setName(name);
        newMerchant.setAccountId(accountId);

        repo.registerMerchant(newMerchant);

        try {
            Client verifyMerchant = repo.getMerchantByAccountId(accountId);
            assertEquals(cpr, verifyMerchant.getCpr());
            assertEquals(name, verifyMerchant.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("the merchant requests the account deregistration")
    public void the_merchant_requests_the_account_deregistration() {
        CorrelationId correlationId = CorrelationId.randomId();
        eventMerchantDeregistraionRequested = new Event("MerchantDeregistrationRequested", new Object[] { newMerchant.getId(), correlationId });
        service.handleMerchantDeregistrationEvent(eventMerchantDeregistraionRequested);
    }

    @Then("the merchant account is successfully deleted")
    public void the_merchant_account_is_successfully_deleted() {
        String accountId = newMerchant.getAccountId();
        try {
            repo.getMerchantByAccountId(accountId);
        } catch (Exception e) {
            assertEquals("Merchant not found", e.getMessage());
        }
    }

    @When("the customer is searched by account id {string}")
    public void the_customer_is_searched_by_account_id(String accountId) {
        try {
            repo.getCustomerByAccountId(accountId);
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the customer is not found")
    public void the_customer_is_not_found() {
        if (exception == null) {
            throw new RuntimeException("Expected exception");
        }
        assertEquals("Customer not found", exception.getMessage());
    }

    @When("the customer is searched by id {string}")
    public void the_customer_is_searched_by_id(String id) {
        try {
            repo.getCustomer(id);
        } catch (Exception e) {
            exception = e;
        }
    }


    @When ("the merchant is searched by account id {string}")
    public void the_merchant_is_searched_by_account_id(String accountId) {
        try {
            repo.getMerchantByAccountId(accountId);
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the merchant is not found")
    public void the_merchant_is_not_found() {
        if (exception == null) {
            throw new RuntimeException("Expected exception");
        }
        assertEquals("Merchant not found", exception.getMessage());
    }

    @When("the merchant is searched by id {string}")
    public void the_merchant_is_searched_by_id(String id) {
        try {
            repo.getMerchant(id);
        } catch (Exception e) {
            exception = e;
        }
    }

    @After
    public void after() {
        try {
            repo.deregisterCustomer(newCustomer.getId());
            repo.deregisterMerchant(newMerchant.getId());
            exception = null; // reset any exceptions
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    @Given("the merchant is not registered with DTU Pay")
    public void theMerchantIsNotRegisteredWithDTUPay() {
        repo.deregisterMerchant(merchant.getId());
    }
    
    @And("the event contains the error message {string}")
    public void theEventContainsTheErrorMessage(String errorMessage) {
        assertEquals(errorMessage, publishedEvent.join().getArgument(0, String.class));
    }
}