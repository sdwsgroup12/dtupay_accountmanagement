# Cesare s232435
Feature: Account Management feature

  Scenario: Successful customer account registration
    Given there is a customer not registered with CPR "123", name "test customer", account id "abcd"
    When the customer requests an account registration
    Then the "CustomerRegistered" event is published
    Then the customer account is successfully created

  Scenario: Successful merchant account registration
    Given there is a merchant not registered with CPR "1234", name "test merchant", account id "abcd1"
    When the merchant requests an account registration
    Then the "MerchantRegistered" event is published
    Then the merchant account is successfully created

  Scenario: Successful customer account deregistration
    Given there is a customer registered with CPR "12345", name "test customer", account id "abcd12"
    When the customer requests the account deregistration
    Then the "CustomerDeregistered" event is published
    Then the customer account is successfully deleted

  Scenario: Successful merchant account deregistration
    Given there is a merchant registered with CPR "123456", name "test merchant", account id "abcd123"
    When the merchant requests the account deregistration
    Then the "MerchantDeregistered" event is published
    Then the merchant account is successfully deleted

  Scenario: Failing customer search
    Given there is a customer not registered with CPR "1234567", name "test customer", account id "abcd1234"
    When the customer is searched by account id "abcd1234"
    Then the customer is not found
    When the customer is searched by id "qwer-asdf-zxcv"
    Then the customer is not found

  Scenario: Failing merchant search
    Given there is a merchant not registered with CPR "12345678", name "test merchant", account id "abcde12345"
    When the merchant is searched by account id "abcde12345"
    Then the merchant is not found
    When the merchant is searched by id "qwer-asdf-zxcv-1234"
    Then the merchant is not found

  Scenario: Successful merchant account id retrieved
    Given there is a "PaymentRequested" event received
    When the PaymentRequested event is handled
    Then the "MerchantBankAccountRetrieved" event is published

  Scenario: Successful customer account id retrieved
    Given there is a "TokenValidated" event received
    When the TokenValidated event is handled
    Then the "CustomerBankAccountRetrieved" event is published

  Scenario: Failing merchant account id retrieved
    Given there is a "PaymentRequested" event received
    And the merchant is not registered with DTU Pay
    When the PaymentRequested event is handled
    Then the "MerchantBankAccountNotRetrieved" event is published
