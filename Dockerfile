FROM eclipse-temurin:21 as jre-build
COPY target/lib /usr/src/lib
COPY /target/DTUPay_accountmanagement-0.0.1.jar /usr/src/
WORKDIR /usr/src/
CMD java -Xmx32m -jar DTUPay_accountmanagement-0.0.1.jar
